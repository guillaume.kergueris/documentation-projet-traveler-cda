#include <stdlib.h>
#include <stdio.h>


int main(int argc, char *argv[]){
    int l = 3;
    char* test = "try";
    printf("%s, %c, %c, %c, %c\n", test, test[0], test[1], test[2], test[4]);
    printf("ils ont respectivement comme addresse : %p, %p, %p, %p, %p \n", &test, &test[0], &test[1], &test[2], &test[4]);
    printf("\n\naddresse de test en mem : %p\n", &test);
    char* copy = NULL;
    /* Allocation de la mémoire */
    copy = (char *) malloc( l * sizeof(char) );
    if( copy == NULL ){
        exit(0); // On arrête tout
    }
    printf("copy a respectivement comme addresse : %p, %p, %p, %p, %p \n", &copy, &copy[0], &copy[1], &copy[2], &copy[4]);
    scanf("%s", copy);
    /* On test que ca marche */
    printf( "La chaine contient : %s, %c, %c\n", copy, copy[1], copy[2] );

    /* Toujours libérer la mémoire après utilisation */
    free(copy);
}