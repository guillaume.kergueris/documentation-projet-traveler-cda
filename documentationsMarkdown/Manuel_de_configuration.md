# Manuel de configuration

Afin de pouvoir lancer, tester le site web de traveler. Vous devez possèder Docker [lien téléchargement](https://www.docker.com/get-started) (veuillez choisir la version de docker desktop correspondant à votre OS).

Une fois Docker téléchargé et installé il vous faudra vous positionner à la racine du projet. Ici deux choix s'offrent à vous :
- Soit vous possédez la commande ```Make``` alors il vous suffira de faire ```make build-docker``` qui va appeller docker avec la commande suivante : ```docker-compose -f docker/docker-compose.yml up```
- soit vous devrez vous positionner au niveau du dossier ```docker``` et écrivez directement ```docker-compose up```

```docker-compose up``` fait appel au fichier docker-compose.yml et crée des ```conteneurs``` qui sont des mini linux (Docker utilise des images de Linux Alpine qui fait de base 4 mo). Ces conteneurs vont, une fois créés, lancer le serveur Nginx/php-fpm.

```Attention, il est possible de lancer la commande docker-compose up avec l'attribut "-d" afin de l'executer en tâche de fond. Il est déconseillé de faire cela car on n'a pas de vision sur l'état des conteneurs à leur création. ```

```Cependant en lançant la commande de la sorte il faudra laisser la console utilisée ouverte car elle maintient le serveur. La supprimer stoppe les conteneurs.```

Une fois l'étape docker terminée il vous suffit de vous connecter dans votre navigateur sur l'adresse [localhost:8080](https://localhost:8080), vous devriez arriver directement sur la page utilisateur qui va vous permettre d'utiliser ```traveler```.
