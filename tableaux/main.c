#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
    // tableau déclaration une a une
    int n[5];
    n[0] = 12;
    n[1] = 1;
    n[2] = 3;
    n[3] = 6;
    n[4] = 6;
    printf("les nombres sont : %d, %d, %d, %d, %d\n", n[0], n[1], n[2], n[3], n[4]);
    // tab déclaration en une seule fois
    int m[3] = {5, 1, 7};
    printf("Pour le second tableau : %d, %d, %d\n\n", m[0], m[1], m[2]);

    // exemple avec malloc / pointeurs
    int* tab;
    // on redimmensionne le tab en 4
    tab = (int *) malloc( 4 * sizeof(int));
    tab[3] = 43; // les autres valeurs seront initialisées à 0 par défault
    printf("le nombre voulu est: %d\n", tab[3]);
    printf("exemple de valeur initialisée à 0 : %d\n", tab[2]);

    int i = 0;
    int j[3];
    while(i < 3)
    {
        j[i] = i;
        printf("%d", j[i]);
        i++;
    }
    return 1;
}