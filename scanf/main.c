#include <stdio.h>
#include <stdlib.h>


int main(int argc, char *argv[]){
    // tableau déclaration une a une
    int n[5] = {0,0,0,0,0};

    int i = 0;
    int j = sizeof(n)/sizeof(n[0]);
    printf("valeur de j : %d\n", j);
    for(i = 0 ; i < j; i++)
    {
        printf("choisir une valeur dans notre tableau : ");
        scanf("%d", &n[i]);
        while(getchar()!='\n');  //On vide le buffer
        printf("dans l'index %d du tableau n on vient d'ajouter la valeur : %d\n", i, n[i]);
    }
    return 1;
}