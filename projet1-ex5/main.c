#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#define MAX_LENGTH 110 // MAX_LENGTH correspond à la longueur max des lignes

// structure dans laquelle on va stocker les infos des villes
struct ville
{
    char Nom[50];
    int CodePostal;
    int SiretINSEE;
    double Longitude;
    double Latitude;
};



int main(int argc, char *argv[]){
    char buff[255];
    char path_to_files[] = "files/";
    char nomfichier[] = "villes_france.txt";
    size_t fullSize;
    printf("Parsing du fichier %s\n", nomfichier);
    /* The last +1 is for the last nul ASCII code for the string */
    fullSize = strlen( path_to_files ) + strlen( nomfichier ) + 1;

    // on créé notre nouvelle string
    char * mon_path;
    mon_path = (char *) malloc( fullSize);
    strcpy(mon_path, path_to_files);
    strcat(mon_path, nomfichier);
    int i = 0;
    // Allocation des espaces mémoire nécessaires sur le tas :
    // allocation de 3 pointeurs
    char **tab_chaine = (char **)malloc(3*sizeof(char*));
    for (i=0 ; i<3 ; i++)
    {
        // allocation pour 10 caractères + 1 caractère nul final
        tab_chaine[i] = (char*)malloc(181);
    }
    printf("chemin du fichier : %s\n", mon_path);
    FILE *open_file  = fopen(mon_path, "r"); // read only
    // test for files not existing.
    if (! open_file)
    {
        printf("Error! Could not open file\n");
        exit(-1); // must include stdlib.h
    }
    int n = 3;
    int ch= 0;
    int lines = 0;
    while(!feof(open_file))
    {
        ch = fgetc(open_file);
        if(ch == '\n')
        {
            lines++;
        }
    }
    printf("Il y a %d lignes dans ce fichier \n", lines);

    int j = lines;
    lines = 0;
    struct ville villes[lines];
    rewind(open_file);
    char * buffer = (char *) malloc( MAX_LENGTH );
    while ( ! feof( open_file ) ) {
        fgets( buffer, MAX_LENGTH, open_file );
        if ( ferror( open_file ) ) {
            fprintf( stderr, "Reading error with code %d\n", errno );
            break;
        }
        if (lines != 0)
        {
            int i = 0; // valeur d'incrementation
            puts( buffer );
            char * strToken = strtok(buffer, "\t");
            while ( strToken != NULL )
            {
                switch (i)
                {
                case 0:
                    strcpy(villes[j].Nom, strToken);
                    break;
                case 1:
                    villes[j].CodePostal = atoi(strToken);
                    break;
                case 2:
                    villes[j].SiretINSEE = atoi(strToken);
                    break;
                case 3:
                    villes[j].Longitude = atof(strToken);
                    break;
                case 4:
                    villes[j].Latitude = atof(strToken);
                    break;
                default:
                    break;
                }

                //printf ( "%s\n", strToken );
                // On demande le token suivant.
                strToken = strtok ( NULL, "\t" );
                i++;
            }
        }
        j++;
        lines++;

    }
    printf("%s, %d, %d, %lf, %lf", villes[0].Nom, villes[0].CodePostal, villes[0].SiretINSEE, villes[0].Longitude, villes[0].Latitude);

    free( buffer );
    fclose( open_file );
    free( mon_path );
    /*exit(0);
    int codep = 0;
    int f = 0;
    printf("Maintenant que l'on vient de récupérer toutes les données, quel CP recherchez-vous? ");
    scanf("%d", &codep);
    //while(getchar()!='\n');  //On vide le buffer
    for(f=0; f< n; f++)
    {
        if (villes[f].CodePostal == codep)
        {
            printf("%s, %d, %d, %f, %f", villes[f].Nom, villes[f].CodePostal, villes[f].SiretINSEE, villes[f].Longitude, villes[f].Latitude);
        }
    }*/
    return 0;
}