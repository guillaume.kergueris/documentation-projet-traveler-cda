# Sécurité projet traveler

## Sommaire

- [Sécurité projet traveler](#sécurité-projet-traveler)
  - [Sommaire](#sommaire)
  - [API](#api)
  - [Failles potentielles](#failles-potentielles)

---

## API

La séparation des deux niveaux : serveur-web & API. Permet de faciliter la sécurisation par la séparation des éléments.

## Failles potentielles

Comme le php fait une execution d'un binaire créé en C, si ce binaire est corrompu, modifié ou mal rédigé il peut créer une faille à l'intérieur du l'application, voir du site.
