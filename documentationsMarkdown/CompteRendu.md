# COMPTE RENDU

Voici tous les programmes développés jusqu'à aujourd'hui avec leurs notions associées en nom de dossier.

## programmes pour apprendre le C

dans chaques dossiers on peut trouver un fichier ```readme.md``` qui décrit les programmes, leurs fonctionnalités.

## projet traveler

Avec Oucema et Remi nous avons fait un projet qui consiste à utiliser un programme en C qui nous servira d'api et de le lier à un site web php/nginx afin de permettre une iterface utilisateur web.

### Objectif du projet traveler

Le projet traveler fonctionne de manière simple:

- un utilisateur choisi plusieurs codes postaux le permier étant le départ et le dernier l'arrivée de son voyage.
- le code en C retourne au php un texte en format json contenant toutes les villes possèdant les code postaux désignés et leurs détails.
- avec l'interface web l'utilisateur choisi les bonnes villes correspondant aux codes postaux (la problématique étant que plusieurs villes peuvent être sur le même code postal).
- Une fois les bonnes villes sélectionnées, le php donne au second programme les bonnes villes avec leurs noms, latitude et longitudes dans le sens de l'itinéraire de l'utilisateur.
- le second programme calcule alors la distance entre les villes en incluant les étapes et retourne alors la distance parcourue (à vol d'oiseau).

### productions

- faire un une api C qui communiquera avec une interface web.
- produire des documentations

### choix technologiques

- [Language C](https://en.cppreference.com/w/c/language)

!["c"](img/c.png)

- [php](https://www.php.net/)

!["php"](img/php.png)

- [Nginx](https://www.nginx.com/)

!["Nginx"](img/nginx.png)

- [javascript](https://developer.mozilla.org/fr/docs/Web/JavaScript)

!["javascript"](img/javascript.png)

- [React](https://fr.reactjs.org/)

!["react"](img/react.jpeg)

- [docker](https://www.docker.com/)

!["docker"](img/docker.png)

### choix architecturaux

- Modele API & webserveur.

### code source

- gestionnaire de version git
- outil gitlab
- [lien du repo](https://gitlab.com/cdabank/traveler)

### gestion de projet

- utilisation du système d'issues gitlab

!["issues"](img/issuesgitlab.png)

#### ma contribution au projet

- créations des 2 fichiers docker-compose et dockerfile qui permettent :
  - pour le fichier docker-compose de faire fonctionner le serveur linux alpine Nginx/php. Ce dernier à été pensé de la sorte à ce que les fichiers placés dans certains répertoires nommés (exemple ; ```bin, public```, etc) d'être automatiquement transférés vers notre conteneur docker(docker détecte les modification et les envoie en temps réel, CF ```volumes``` docker).
  - pour le ```dockerfile``` de créer une image personnalisée d'un linux alpine et de créer nos binaires avec ce dernier(une fois que le conteneur est actif), les binaires sont ensuite extraits du conteneur pour être ensuite donnés à notre conteneur contenant le serveur. À la fin de l'execution, le conteneur se stoppe, on peut ensuite le détruire manuellement ou alors en utilisant le makefile il se détruit tout seul.
- création du fichier makefile. Ce dernier contient 3 instructions. ```compil, compil-no-cache & build-docker```.
  - ```compil & compil-no-cache``` servent à l'execution du Dockerfile présenté précedemment, lancent le conteneur, récupèrent les résultats et les déposent dans le dossier réservé au serveur, la différence entre les deux commandes est que ```compil-no-cache``` fait toutes ces étapes sans utiliser le cache, ce qui permet de s'assurer que le cache n'est pas responsable des problèmes dans la création de nos binaires.
  - ```build-docker``` permet d'executer la commande ```docker-compose up``` sans avoir à ce déplacer dans l'arborescence du projet. Lire le Markdown/pdf ```Manuel_de_configuration.md``` pour plus d'informations.
- création de l'interface php-api, implémentation de 2 expressions régulières côté serveur afin d'empêcher les injections de fausses données.
- création/modification des fichiers C, implementations de ces derniers dans l'api, ajouts de failsafes, implementation d'un format json valide. (problème avec une virgule de trop dans une boucle while qui rendait notre json défectueux).

## Sources et aides

- [koor (tutoriels C)](https://koor.fr/)
- [docker](https://www.docker.com/)
- [Image docker php](https://hub.docker.com/_/php)
- [Tutoriel docker php Nginx](http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/)
