# Sommaire

- [Sommaire](#sommaire)
  - [Page principale](#page-principale)
  - [Codes postaux](#codes-postaux)
  - [Validation utilisateur](#validation-utilisateur)
  - [Demander le résultat & Total de Kilomètres parcourus](#demander-le-résultat--total-de-kilomètres-parcourus)

---

## Page principale

Pour y accèder rendez-vous sur [ce lien](http://localhost:8080/). Vous devriez obtenir un aperçu de la page ci-dessous:

![Acceuil](img/Acceuil.png)

## Codes postaux

On peut dès à présent saisir les données des deux codes postaux de notre itinéraire et valider en appuyant soit sur ```Entrer``` soit en cliquant sur le bouton ```rechercher```.

![entrees](img/entrees.png)

## Validation utilisateur

Si il y a plusieurs villes correspondant au même Code Postal alors l'utilisateur doit choisir manuellement la ville voulue.

![plusvilles](img/plusvilles.png)

## Demander le résultat & Total de Kilomètres parcourus

Vous devriez normalement obtenir ce visuel si toutes les étapes précédentes ce sont correctement déroulées:

![result0](img/resultat0.png)

En appuyant sur ```Calculer l'itinéraire``` on obtient alors le nombre de kilomètres séparant nos deux villes.

![result1](img/resultat1.png)

Vous pouvez recommencer en appuyant sur ```Changer l'itinéraire```.